cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
    {
        "file": "plugins/com-sarriaroman-photoviewer/www/PhotoViewer.js",
        "id": "com-sarriaroman-photoviewer.PhotoViewer",
        "pluginId": "com-sarriaroman-photoviewer",
        "clobbers": [
            "PhotoViewer"
        ]
    }
];
module.exports.metadata = 
// TOP OF METADATA
{
    "cordova-plugin-whitelist": "1.3.3",
    "cordova-plugin-geolocation": "4.0.1",
    "com-sarriaroman-photoviewer": "1.2.2"
}
// BOTTOM OF METADATA
});